### JUnit Sample Demo

This package includes sample java code integrate with JUnit testing framework.

This package can be compiled with Maven / Ant

JUnit Sample demo integrate with a TCM Automation Host

### How to use sample package to integrate with TCM Automation Host
- First, setup TCM Automation Host. And install JUnit for Java framework in TCM Automation Host.
- Download JUnit sample package and unzip in your directory (eg: D:\Demo\junit-sample).
- Open command line at directory D:\Demo\junit-sample and execute command: *mvn clean compile package test*
- Set up automation host as per TCM instructions
